import './style.css'

export default function Product({item, handleClick, setCartTotal, currentSale}) {

    return (        
        <div className='product' id={item.id}>
            <p>{item.name}</p>
            <p>Categoria - {item.category}</p>
            <p>Preço - R${item.price}</p>
            <button className='add' onClick={()=>{
                handleClick(item.id)
                setCartTotal(currentSale.reduce((acc, curr)=> acc+curr.price, 0).toFixed(2))
            }}>Adicionar</button>
        </div>
    )
}