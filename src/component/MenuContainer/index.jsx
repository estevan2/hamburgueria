import Product from "../Product";

export default function MenuContainer({state, cartTotal, currentSale, handleClick, setCartTotal}) {
    
    return (
        <div>
            <div className='content'>
                {state.map(item=>(
                    <Product currentSale={currentSale} setCartTotal={setCartTotal} item={item} handleClick={handleClick}></Product>
                ))}
            </div>
            <div className='content'>
                <div>Subtotal - R${cartTotal}</div>
                <div className='content'>
                    {currentSale.map(item=>(
                        <div className='product' id={item.id}>
                            <p>{item.name}</p>
                            <p>Categoria - {item.category}</p>
                            <p>Preço - R${item.price}</p>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}