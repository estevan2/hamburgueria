import './App.css';
import { useState } from 'react'
import MenuContainer from './component/MenuContainer';

function App() {
  const [products, setProducts] = useState([
    { id: 1, name: 'Hamburguer', category: 'Sanduíches', price: 7.99 },
    { id: 2, name: 'X-Burguer', category: 'Sanduíches', price: 8.99 },
    { id: 3, name: 'X-Salada', category: 'Sanduíches', price: 10.99 },
    { id: 4, name: 'Big Kenzie', category: 'Sanduíches', price: 16.99 },
    { id: 5, name: 'Guaraná', category: 'Bebidas', price: 4.99 },
    { id: 6, name: 'Coca', category: 'Bebidas', price: 4.99 },
    { id: 7, name: 'Fanta', category: 'Bebidas', price: 4.99 },
  ]); 

  const [filteredProducts, setFilteredProducts] = useState(products);

  const [currentSale, setCurrentSale] = useState([])
  const [cartTotal, setCartTotal] = useState(0)

  const showProducts = (input) => {
    const filtered = products.filter(item=>item.name===input)
    setFilteredProducts(filtered)
  }

  const handleClick = (id) => {
    const finded = products.find(item=>item.id === id)
    setCurrentSale([...currentSale, finded])
    setFilteredProducts(filteredProducts.filter(item => item.id !== id))
  }

  const [currentSearch, setCurrentSearch] = useState('')

  const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return (
    <main>
      <div className='search'>
        <input type='text' onChange={event=>{
          setCurrentSearch(capitalizeFirstLetter(event.target.value.trim().toLowerCase()))
        }} />
        <button onClick={()=>showProducts(currentSearch)}>Pesquisar</button>
      </div>
      <MenuContainer state={filteredProducts} cartTotal={cartTotal} setCartTotal={setCartTotal} currentSale={currentSale} handleClick={handleClick}></MenuContainer>
    </main>
  );
}

export default App;
